package com.lorenacadena.productos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll(){
        return productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id){
        return productRepository.findById(id);
    }

    public ProductModel sava(ProductModel product){
        return productRepository.save( product );
    }

    public  Boolean delete( ProductModel product ){
        try {
            productRepository.delete( product );
            return true;
        }catch (Exception ex){
            return false;
        }
    }
}
