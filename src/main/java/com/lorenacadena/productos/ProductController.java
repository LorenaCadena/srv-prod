package com.lorenacadena.productos;

import com.lorenacadena.productos.ProductModel;
import com.lorenacadena.productos.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/productos")
    public List<ProductModel> getProductos(){
        return productService.findAll();
    }

    @PostMapping("/productos")
    public ProductModel postProducto(@RequestBody ProductModel product){
        return productService.sava( product );
    }

    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductModel product){
        productService.sava(product);
    }

    @DeleteMapping("/productos")
    public boolean deleteProducto(@RequestBody ProductModel product){
        return productService.delete( product );
    }
}
